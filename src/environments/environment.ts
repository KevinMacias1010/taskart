// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAiKLQC2jFBpqHUYHoeJL0p141XsHWX7Pc",
    authDomain: "administrador-tareas.firebaseapp.com",
    databaseURL: "https://administrador-tareas.firebaseio.com",
    projectId: "administrador-tareas",
    storageBucket: "administrador-tareas.appspot.com",
    messagingSenderId: "700094994052"
  }
};
