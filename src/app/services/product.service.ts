import { Injectable } from '@angular/core';

// Firebase
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

// Model
import { Product } from '../models/product';

@Injectable()
export class ProductService {

  productList: AngularFireList<any>;
  selectedProduct: Product = new Product();

  constructor(private firebase: AngularFireDatabase) { }

  getProducts()
  {
    return this.productList = this.firebase.list('tareas');
  }

  insertProduct(product: Product)
  {
    this.productList.push({
      nombre: product.nombre,
      fechainicio: product.fechainicio,
      fechafinal: product.fechafinal,
      prioridad: product.prioridad,
      descripcion: product.descripcion

    });
  }

  updateProduct(product: Product)
  {
    this.productList.update(product.$key, {
      nombre: product.nombre,
      fechainicio: product.fechainicio,
      fechafinal: product.fechafinal,
      prioridad: product.prioridad,
      descripcion: product.descripcion
    });
  }

  deleteProduct($key: string)
  {
    this.productList.remove($key);
  }
}
