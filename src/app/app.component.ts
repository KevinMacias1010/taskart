import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit{
  title = 'app';
  sw: boolean;
  ngOnInit() {
    this.sw= false;
    
  }
    abrirMenu() {
      this.sw = !this.sw;
    }
    cerrarMenu() {
      this.sw = false;
    }


}


 
