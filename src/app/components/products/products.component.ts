import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  list: string[] = ["", "", ""]
  sw: boolean;

  constructor() { }

  ngOnInit() {
    this.sw= true;
    
  }

  abrirA() {
    this.sw = !this.sw;
  }

}
