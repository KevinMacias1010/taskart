import { Component, OnInit } from '@angular/core';

// model
import { Product } from '../../../models/product';

// service
import { ProductService } from '../../../services/product.service';

// toastr
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  productList: Product[];
  variable: string;

  constructor(
    private productService: ProductService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.variable = "";
    return this.productService.getProducts()
      .snapshotChanges().subscribe(item => {
        this.productList = [];
        item.forEach(element => {
          let x = element.payload.toJSON();
          x["$key"] = element.key;
          this.productList.push(x as Product);
        });
      });


  }

  buscarTareas() {
    let input, filter, table, tr, td, i;
    input = document.getElementById("campo-buscar");
    filter = input.value.toUpperCase();
    table = document.getElementById("lista-tareas");
    tr = table.getElementsByTagName("tr");
  
    
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[0];
      if (td) {
        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          
          tr[i].style.display = "none";          
        }
      } 
    }
  }

  onEdit(product: Product) {
    this.productService.selectedProduct = Object.assign({}, product);
    this.toastr.success('Tarea seleccionada con exito!', 'Tarea seleccionada')
    window.scroll(0,0);
  }

  onDelete($key: string) {
    if(confirm('¿Estas seguro que quieres finalizar esta tarea (despues de finalizada no se podrá recuperar) ?')) {
      this.productService.deleteProduct($key);
      this.toastr.success('Tarea finalizada con exito!', 'Tarea finalizada');
      
    }
  }

}
